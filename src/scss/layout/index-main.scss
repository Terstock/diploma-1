.main-info-grid {
  display: flex;
  margin-top: 30px;
  margin-bottom: 20px;
  display: grid;
  grid-template-columns: 38% 60%;
  grid-column-gap: 30px;

  @media screen and (max-width: 1360px) {
    grid-template-columns: 1fr;
    grid-template-rows: 46% 52%;
    row-gap: 20px;
  }

  // @media screen and (max-width: 600px) {
  //   grid-template-rows: 55% 42%;
  // }
}
/////////////////
/// MAIN FILTERS
////////////////

.main__filters {
  height: auto;
  padding: 20px;
  background-color: $powder-blue;
  border-radius: 5px;
}

.main__filters-list {
  display: flex;
  flex-direction: column;
  gap: 25px;
}

.main__filters-element {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.main__filters-element-link {
  display: flex;
  gap: 15px;
  align-items: center;
  text-decoration: none;
  color: $black;
  width: fit-content;
}

.main__filters-element-link:hover {
  text-decoration: underline;
  cursor: pointer;
}

.main__filters-category-title {
  color: $eclipse;
  font-family: 'Poppins', sans-serif;
  text-align: center;
  @include text-properties(400, normal);
  font-size: 24px;
  display: flex;
  flex-direction: row;
  @media screen and (max-width: 760px) {
    font-size: 22px;
  }
}

.main__filters-icon {
  @include size(35px, 35px);
  @media screen and (max-width: 760px) {
    @include size(25px, 25px);
  }
}

.main__filters-category-numbers {
  color: $eclipse;
  font-family: 'Poppins', sans-serif;
  text-align: center;
  @include text-properties(400, normal);
  font-size: clamp(18px, 5.6vw, 20px);
}
//////////////
/// PAGE-BANNER
///////////////

.main__banner {
  height: 520px;
  @include border-properties(1px solid $middle-grey, 5px);
}

.main__banner-slideshow-container {
  position: relative;
  height: 520px;
}

.main__banner-slides {
  display: none;
  height: fit-content;
  position: relative;
}

.main__banner-img {
  @include size(100%, 520px);
}

.main__banner-sign-container {
  position: absolute;
  background-repeat: no-repeat;
  background-size: cover;
  transform: translate(-50%, -50%);
  top: 50%;
  left: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 10px;
}

.main__banner-sign {
  @include size(400px, 150px);
  @media screen and (max-width: 420px) {
    @include size(300px, 100px);
  }
}

.main__banner-sign-text {
  color: $white;
  font-family: 'Poppins', sans-serif;
  text-align: center;
  @include text-properties(700, normal);
  font-size: 30px;

  @media screen and (max-width: 560px) {
    font-size: 22px;
  }
}

.main__banner-prev,
.main__banner-next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: $white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

.main__banner-next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

.main__banner-prev:hover,
.main__banner-next:hover {
  background-color: $black-back;
}

.main__banner-dot-class {
  position: relative;
  bottom: 25px;
}

.main__banner-dot {
  cursor: pointer;
  @include size(13px, 13px);
  background-color: $white;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}
.active,
.main__banner-dot:hover {
  background-color: $gold;
}

.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

//////////////////
/// POPULAR THEMES
//////////////////

.main__popular-themes {
  display: flex;
  flex-direction: column;
}

.main__popular-themes-banner {
  margin-top: 50px;
  height: 200px;
  background-image: url(/images/pictures/popular-themes3.jpg);
  margin-bottom: 50px;
  background-repeat: no-repeat;
  background-size: cover;
  background-color: $black-back;

  @media screen and (max-width: 1199px) {
    height: 100px;
  }
}

.overlay {
  display: flex;
  @include size(100%, 100%);
  background-color: $black-back;
  justify-content: center;
  align-items: center;
}

.overlay:hover {
  background-color: $black-lighter-back;
  transition: background-color 0.5s ease;
  cursor: pointer;
}

.main__popular-themes-text {
  color: $white;
  font-family: 'Poppins', sans-serif;
  text-align: center;
  @include text-properties(600, normal);
  font-size: 32px;
  @media screen and (max-width: 420px) {
    font-size: 24px;
  }
}

.main__popular-themes-examples {
  display: flex;
  margin-bottom: 50px;
}

.main__popular-themes-examples-content {
  display: flex;
  justify-content: space-between;
  width: 100%;

  @media screen and (max-width: 1600px) {
    display: grid;
    grid-template-columns: 1fr;
    grid-template-rows: repeat(3, 1fr);
    row-gap: 20px;
  }
}

.popular-themes-example-1 {
  display: flex;
  justify-content: center;
  align-items: center;
  @include size(450px, 200px);
  border-radius: 5px;
  background-image: url(/images/pictures/theme-1.jpg);
  background-repeat: no-repeat;
  background-size: cover;

  @media screen and (max-width: 1600px) {
    @include size(100%, 100px);
  }
}
.popular-themes-example-2 {
  display: flex;
  justify-content: center;
  align-items: center;
  @include size(450px, 200px);
  border-radius: 5px;
  background-image: url(/images/pictures/theme-2.jpg);
  background-repeat: no-repeat;
  background-size: cover;

  @media screen and (max-width: 1600px) {
    @include size(100%, 100px);
  }
}
.popular-themes-example-3 {
  display: flex;
  justify-content: center;
  align-items: center;
  @include size(450px, 200px);
  border-radius: 5px;
  background-image: url(/images/pictures/theme-3.jpg);
  background-repeat: no-repeat;
  background-size: cover;

  @media screen and (max-width: 1600px) {
    @include size(100%, 100px);
  }
}

/////////////////////////////////////////////
/// /////////////////////////////////////////////
/// PRODUCTS
/// /////////////////////////////////////////////
/// /////////////////////////////////////////////

.main__shop-products {
  margin-bottom: 50px;
}

.main__shop-products-content {
  display: flex;
  flex-direction: column;
  gap: 30px;
  margin-bottom: 30px;
}

.main__shop-products-titlus {
  display: flex;
  gap: 20px;
  align-items: center;
}

.main__shop-products-class {
  color: $black;
  font-family: 'Poppins', sans-serif;
  text-align: left;
  @include text-properties(600, normal);
  font-size: 32px;
}

.main__shop-products-list {
  display: grid;
  grid-template-columns: repeat(auto-fill, 330px);
  width: 100%;
  justify-content: center;
  justify-items: center;
  column-gap: 30px;
}

.card {
  min-height: 350px;
  display: flex;
  flex-direction: column;
  border-radius: 4px;
  position: relative;
}

.main__shop-products-card-image {
  display: block;
  cursor: pointer;
  padding-bottom: 30px;
}

.main__shop-products-card-image > img {
  @include size(100%, 100%);
  object-fit: contain;
  transition: 0.2s;
}

.main__shop-products-price {
  color: $black;
  font-family: 'Lato', sans-serif;
  text-align: left;
  @include text-properties(400, normal);
  font-size: 32px;
}

.card__price--discount {
  font-family: 'Lato', sans-serif;
  @include text-properties(400, normal);
  font-size: 32px;
  color: $bright-red;
  display: flex;
  flex-wrap: wrap-reverse;
}

.card__price--common {
  font-family: 'Lato', sans-serif;
  @include text-properties(400, normal);
  font-size: 32px;
  color: $grey;
  text-decoration: line-through;
  display: flex;
}

.card__price--discount:after {
  content: '₴';
  margin-left: 4px;
  position: relative;
}
.card__price--common:after {
  content: '₴';
  margin-left: 4px;
  position: relative;
}

.card__price--discount-en {
  font-family: 'Lato', sans-serif;
  @include text-properties(400, normal);
  font-size: 32px;
  color: $bright-red;
  display: flex;
  flex-wrap: wrap-reverse;
}

.card__price--common-en {
  font-family: 'Lato', sans-serif;
  @include text-properties(400, normal);
  font-size: 32px;
  color: $grey;
  text-decoration: line-through;
  display: flex;
}

.card__price--discount-en:after {
  content: '$';
  margin-left: 4px;
  position: relative;
}
.card__price--common-en:after {
  content: '$';
  margin-left: 4px;
  position: relative;
}

.card__title {
  display: block;
  margin-bottom: 10px;
  font-size: 17px;
  @include text-properties(400, 150%);
  color: $charcoal;
}
.card__title:hover {
  color: $dark-blue-1;
}
.main__shop-products-card-add {
  padding: 10px 20px;
  font-family: 'Lato', sans-serif;
  text-align: center;
  @include color-back($white, $bright-blue);
  @include text-properties(400, normal);
  font-size: 20px;
  width: 100%;
  @media screen and (max-width: 420px) {
    font-size: 18px;
  }
}

.main__shop-products-card-add:hover {
  border: 1px solid $card-dark-blue;
  background-color: $card-dark-blue;
  color: $white;
  cursor: pointer;
}

.main__shop-products-absent {
  border: 1px solid $grey;
  padding: 10px 20px;
  font-family: 'Lato', sans-serif;
  text-align: center;
  @include color-back($white, $grey);
  @include text-properties(400, normal);
  font-size: 20px;
  width: 100%;
}

.main__shop-products-absent:hover {
  cursor: pointer;
  background-color: $border-grey;
}

.grey-font {
  color: $grey;
}

.main__shop-products-link {
  text-decoration: none;
  color: $black;
  cursor: pointer;
}

.main__shop-products-link:hover {
  text-decoration: underline;
}

.main__shop-products-product-content {
  display: flex;
  height: 560px;
  @include border-properties(1px solid $middle-grey, 5px);
  flex-direction: column;
  justify-content: space-between;
  padding: 14px;
}

.main__shop-products-photo {
  @include size(350px, 350px);
}

.main__shop-products-container {
  position: absolute;
  display: flex;
  gap: 4px;
}

.main__shop-products-category-hit {
  position: relative;
  display: flex;
  @include category-details(4px, $purple, $white, center);
  @include padding(4px, 8px, 4px, 8px);
  @include text-properties(600, normal);
  font-family: 'Lato', sans-serif;
  font-size: 16;
}

.main__shop-products-category-new {
  position: relative;
  display: flex;
  @include category-details(4px, $green, $white, center);
  @include padding(4px, 8px, 4px, 8px);
  @include text-properties(600, normal);
  font-family: 'Lato', sans-serif;
  font-size: 16;
}

.main__shop-products-category-discount-small {
  position: relative;
  display: flex;
  @include category-details(4px, $dark-gold, $white, center);
  @include padding(4px, 8px, 4px, 8px);
  @include text-properties(600, normal);
  font-family: 'Lato', sans-serif;
  font-size: 16;
}

.main__shop-products-category-discount-big {
  position: relative;
  display: flex;
  @include category-details(4px, $red, $white, center);
  @include padding(4px, 8px, 4px, 8px);
  @include text-properties(600, normal);
  font-family: 'Lato', sans-serif;
  font-size: 16;
}

.main__shop-products-info {
  display: flex;
  flex-direction: column;
  gap: 5px;
}

.main__shop-products-description {
  color: $black;
  font-family: 'Lato', sans-serif;
  text-align: left;
  @include text-properties(600, normal);
  font-size: 20px;
}

.no-underline:hover {
  text-decoration: none;
}

.main__shop-products-price-content {
  display: flex;
  justify-content: start;
  gap: 30px;
}

.main__shop-products-buying {
  display: flex;
  gap: 20px;
  justify-content: center;
  padding-top: 20px;
  cursor: pointer;
}

.main__shop-products-to-basket {
  border: 1px solid $middle-grey;
  @include color-back($white, $bright-blue);
  @include text-properties(400, normal);
  @include padding(10px, 20px, 10px, 20px);
  font-family: 'Lato', sans-serif;
  text-align: center;
  font-size: 20px;
  width: 100%;
}

.main__shop-products-to-basket:hover,
.main__shop-products-buy:hover {
  background: $card-dark-blue;
}

.main__shop-products-buy {
  border: 1px solid $middle-grey;
  @include color-back($white, $bright-blue);
  @include padding(10px, 20px, 10px, 20px);
  @include text-properties(400, normal);
  font-family: 'Lato', sans-serif;
  text-align: center;
  font-size: 20px;
  width: 100%;
  @media screen and (max-width: 420px) {
    font-size: 18px;
  }
}

/////////////////////////////////////////////
/// /////////////////////////////////////////////
/// PRODUCTS: BUY PRODUCT
/// /////////////////////////////////////////////
/// /////////////////////////////////////////////

.main__shop-products-order {
  position: fixed;
  top: 0;
  left: 0;
  background-color: $black-lighter-back;
  z-index: 10;
  @include size(100%, 100%);
  display: none;
  justify-content: center;
  align-items: center;
  user-select: none;
  overflow-y: scroll;
}

.main__shop-products-order-container {
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  max-width: 600px;
  min-height: 300px;
  gap: 20px;
  background-color: $white;
  box-shadow: 1px 2px 4px $box-shadow;
  border-radius: 4px;
  position: relative;
  padding: 20px;
  align-items: center;
}

.main__shop-products-order-title {
  color: $black;
  font-family: 'Lato', sans-serif;
  text-align: center;
  @include text-properties(600, normal);
  font-size: 20px;
}

.main__shop-products-order-photo {
  @include size(70%, 280px);
  border: 1px solid $middle-grey;
  background-image: url(/images/pictures/order-completed.jpg);
  background-repeat: no-repeat;
  background-size: cover;
}

.main__shop-products-order-form {
  display: flex;
  flex-direction: column;
  gap: 15px;
  width: 70%;
}

.main__shop-products-order-input {
  width: 100%;
  padding-left: 15px;
  @include color-back($zambezi, $white);
  @include border-properties(1px solid $middle-grey, 5px);
  @include text-properties(400, 250%);
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  justify-content: center;
  opacity: 0.7;
}

.main__shop-products-order-input:hover {
  opacity: 1;
}

.main__shop-products-order-btn {
  font-family: 'Lato', sans-serif;
  text-align: center;
  @include text-properties(400, normal);
  @include color-back($black, $gold);
  font-size: 20px;
  opacity: 0.7;
  border-radius: 5px;
  padding: 12px 0px;
  width: 70%;
  cursor: pointer;
}

.main__shop-products-order-btn:hover {
  opacity: 1;
}

.main__shop-products-order-close {
  position: absolute;
  top: 2%;
  right: 1%;
  cursor: pointer;
  font-size: 18px;
}

/////////////////////////////////////////////
/// /////////////////////////////////////////////
/// CLIENTS AND SUPPLIERS
/// /////////////////////////////////////////////
/// /////////////////////////////////////////////

.main__for-statement {
  display: flex;
  flex-direction: column;
  margin-bottom: 50px;
}

.main__for-statement-content {
  display: flex;
  flex-direction: column;
  gap: 50px;

  @media screen and (max-width: 1200px) {
    row-gap: 20px;
  }
}

.main__for-statement-buyers {
  display: grid;
  grid-template-columns: 2fr 1fr;
  grid-template-rows: 100%;
  column-gap: 30px;

  @media screen and (max-width: 1200px) {
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 1fr;
    row-gap: 20px;
  }
}

.main__for-statement-buyers-info {
  display: flex;
  flex-direction: column;
  gap: 10px;
  @include border-properties(1px solid $middle-grey, 5px);
  height: 260px;
  padding: 20px;
  overflow: scroll;
}

.main__for-statement-buyers-banner {
  height: auto;
  background-image: url(/images/pictures/sellers-banner-antique.jpg);
  background-size: cover;
  background-repeat: no-repeat;
}

.main__for-statement-buyers-title {
  color: $black;
  font-family: 'Poppins', sans-serif;
  text-align: left;
  @include text-properties(400, normal);
  font-size: 40px;
  @media screen and (max-width: 420px) {
    font-size: 28px;
  }
}

.main__for-statement-buyers-text {
  color: $black;
  font-family: 'Poppins', sans-serif;
  text-align: left;
  @include text-properties(400, normal);
  font-size: 20px;
  @media screen and (max-width: 420px) {
    font-size: 18px;
  }
}

.main__for-statement-link {
  text-decoration: underline;
}

.main__for-statement-link:hover {
  color: $card-dark-blue;
  cursor: pointer;
}

.main__for-statement-suppliers {
  display: grid;
  grid-template-columns: 1fr 2fr;
  column-gap: 30px;

  @media screen and (max-width: 1200px) {
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 1fr;
    row-gap: 20px;
  }
}

.main__for-statement-suppliers-info {
  display: flex;
  flex-direction: column;
  gap: 10px;
  @include border-properties(1px solid $middle-grey, 5px);
  height: 260px;
  padding: 20px;
  overflow: scroll;
}

.main__for-statement-suppliers-banner {
  height: auto;
  background-image: url(/images/pictures/clients-banner-antique.jpg);
  background-size: cover;
  background-repeat: no-repeat;
}

.main__for-statement-suppliers-title {
  color: $black;
  font-family: 'Poppins', sans-serif;
  text-align: left;
  @include text-properties(400, normal);
  font-size: 40px;
  @media screen and (max-width: 420px) {
    font-size: 28px;
  }
}

.main__for-statement-suppliers-text {
  color: $black;
  font-family: 'Poppins', sans-serif;
  text-align: left;
  @include text-properties(400, normal);
  font-size: 20px;
  @media screen and (max-width: 420px) {
    font-size: 18px;
  }
}

/////////////////////////////////////////////
/// /////////////////////////////////////////////
/// ABOUT COMPANY
/// /////////////////////////////////////////////
/// /////////////////////////////////////////////

.main__about-company {
  margin-bottom: 50px;
}

.main__about-company-info {
  display: flex;
  flex-direction: column;
  padding: 20px;
  @include border-properties(1px solid $middle-grey, 5px);
  gap: 30px;
  overflow: scroll;
}

.main__about-company-title {
  color: $black;
  font-family: 'Poppins', sans-serif;
  text-align: left;
  @include text-properties(400, normal);
  font-size: 40px;
  @media screen and (max-width: 420px) {
    font-size: 28px;
    text-align: left;
  }
}

.main__about-company-text {
  color: $black;
  font-family: 'Poppins', sans-serif;
  text-align: left;
  @include text-properties(400, normal);
  font-size: 20px;
  @media screen and (max-width: 420px) {
    font-size: 18px;
  }
}

.main__about-company-advantages {
  display: grid;
  justify-content: space-around;
  align-items: center;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: 1fr;
  column-gap: 30px;
  @media screen and (max-width: 900px) {
    grid-template-columns: 1fr;
    grid-template-rows: repeat(3, 1fr);
    row-gap: 20px;
  }
}

.main__about-company-list {
  display: flex;
  flex-direction: column;
  gap: 20px;
  justify-content: center;
  align-items: center;
  @media screen and (max-width: 900px) {
    justify-content: left;
    align-items: start;
  }
}

.main__about-company-element {
  width: fit-content;
  text-align: left;
}

.main__about-company-content {
  display: flex;
  align-items: center;
  gap: 25px;
  justify-content: center;
}

.main__about-company-photo {
  @include size(50px, 50px);

  @media screen and (max-width: 760px) {
    @include size(40px, 40px);
  }
}

.main__about-company-advantage {
  color: $black;
  font-family: 'Poppins', sans-serif;
  text-align: center;
  @include text-properties(400, normal);
  font-size: clamp(15px, 4.6vw, 24px);
}
