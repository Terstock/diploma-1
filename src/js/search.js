function redirectToPage(event) {
  event.preventDefault();

  const query = document.querySelector("#searchInput").value.toLowerCase();

  switch (query) {
    case "головна сторінка":
      window.open("index.html", "_blank");
      break;
    case "про нас":
      window.open("about-us.html", "_blank");
      break;
    case "контакти":
      window.open("contacts.html", "_blank");
      break;
    case "оплата і доставка":
      window.open("payment-and-delivery.html", "_blank");
      break;
    case "відгуки клієнтів":
      window.open("clients-reviews.html", "_blank");
      break;
    case "наші гарантії":
      window.open("our-guarantees.html", "_blank");
      break;
    case "постачальникам та посередникам":
      window.open("suppliers-and-intermediaries.html", "_blank");
      break;
    case "home page":
      window.open("index-english.html", "_blank");
      break;
    case "about us":
      window.open("about-us-en.html", "_blank");
      break;
    case "contacts":
      window.open("contacts-en.html", "_blank");
      break;
    case "payment and delivery":
      window.open("payment-and-delivery-en.html", "_blank");
      break;
    case "customer reviews":
      window.open("clients-reviews-en.html", "_blank");
      break;
    case "our guarantees":
      window.open("our-guarantees-en.html", "_blank");
      break;
    case "suppliers and intermediaries":
      window.open("suppliers-and-intermediaries-en.html", "_blank");
      break;
    default:
      window.open("index.html", "_blank");
      break;
  }
}

///SEARCH ADAPTIVE

let searchOpenBtn = document.querySelector("#search-open-button");
let catalogOpenBtn = document.querySelector("#catalog-open-btn");
let adaptiveLogo = document.querySelector("#adaptive-logo");
let cartFirst = document.querySelector("#cart");
let callBtnSecond = document.querySelector("#call-us-button-2");
let changeDiv = document.querySelector("#currensy-and-language");
let secondSearch = document.querySelector(".header-section__search-class");
let searchCloseBtn = document.querySelector("#search-form-close");

searchOpenBtn.addEventListener("click", function () {
  searchOpenBtn.style.display = "none";
  catalogOpenBtn.style.display = "none";
  adaptiveLogo.style.display = "none";
  cartFirst.style.display = "none";
  callBtnSecond.style.display = "none";
  changeDiv.style.display = "none";
  secondSearch.style.display = "flex";
  searchCloseBtn.style.display = "block";
});

searchCloseBtn.addEventListener("click", function () {
  searchOpenBtn.style.display = "block";
  catalogOpenBtn.style.display = "block";
  adaptiveLogo.style.display = "block";
  cartFirst.style.display = "flex";
  callBtnSecond.style.display = "block";
  changeDiv.style.display = "flex";
  secondSearch.style.display = "none";
  searchCloseBtn.style.display = "none";
});
