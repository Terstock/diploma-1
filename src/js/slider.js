let slideIndex = 1;
let interval;

function startSlider() {
  interval = setInterval(function () {
    plusSlides(1);
  }, 5000);
}

function stopSlider() {
  clearInterval(interval);
}

showSlides(slideIndex);
startSlider();

function plusSlides(n) {
  showSlides((slideIndex += n));
}

function currentSlide(n) {
  showSlides((slideIndex = n));
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("main__banner-slides");
  let dots = document.getElementsByClassName("main__banner-dot");
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";
}

let sliderContainer = document.querySelector(
  ".main__banner-slideshow-container"
);

sliderContainer.addEventListener("mouseover", function () {
  stopSlider();
});

sliderContainer.addEventListener("mouseout", function () {
  startSlider();
});
