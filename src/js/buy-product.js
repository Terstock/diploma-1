const buyButtons = document.querySelectorAll(".main__shop-products-buy");
const orderForm = document.querySelector(".main__shop-products-order");

buyButtons.forEach(function (buyButton) {
  buyButton.addEventListener("click", function () {
    orderForm.style.display = "flex";
    document.body.style.overflow = "unset";
  });
});

let orderBtn = document.querySelector(".main__shop-products-order-close");

orderBtn.addEventListener("click", function () {
  orderForm.style.display = "none";
  document.body.style.overflow = "unset";
});

const form = document.querySelector("#buyForm");

form.addEventListener("submit", function (event) {
  event.preventDefault();

  const nameInput = document.querySelector("#name-input").value;
  const numberInput = document.querySelector("#number-input").value;
  if (nameInput.trim() === "" || numberInput.trim() === "") {
    alert("Будь ласка, заповніть всі поля форми");
    return;
  }

  let badSymbols = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0",
    "!",
    "@",
    "#",
    "$",
    "%",
    "^",
    "&",
    "*",
    ".",
    "/",
    "?",
    ">",
    "<",
    "]",
    "[",
    "(",
    ")",
    "-",
    "+",
    "{",
    "}",
    "=",
  ];

  badNumbers = [
    "!",
    "@",
    "#",
    "$",
    "%",
    "^",
    "&",
    "*",
    ".",
    "/",
    "?",
    ">",
    "<",
    "]",
    "[",
    "(",
    ")",
    "{",
    "}",
    "=",
  ];

  if (badSymbols.some((char) => nameInput.includes(char))) {
    alert("У Вашому імені є заборонені символи!");
  }

  const inputElement = document.getElementById("number-input");
  const inputValue = inputElement.value;
  const numericValue = parseInt(inputValue);

  if (
    isNaN(numericValue) ||
    badNumbers.some((char) => numberInput.includes(char))
  ) {
    alert("Будь ласка, введіть коректний номер!");
  } else {
    form.reset();
    orderForm.style.display = "none";
  }
});
