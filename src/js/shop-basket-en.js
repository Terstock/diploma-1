function toNum(str) {
  const num = Number(str.replace(/ /g, ""));
  return num;
}

function toCurrency(num) {
  const format = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 0,
  }).format(num);
  return format;
}

const cardAddArr = Array.from(
  document.querySelectorAll(".main__shop-products-card-add")
);
const cartNum = document.querySelector("#cart-num");
const cart = document.querySelector("#cart");

class Cart {
  products;
  constructor() {
    this.products = [];
  }
  get count() {
    return this.products.length;
  }
  addProduct(product) {
    this.products.push(product);
  }
  removeProduct(index) {
    this.products.splice(index, 1);
  }
  get cost() {
    const prices = this.products.map((product) => {
      return toNum(product.price);
    });
    const sum = prices.reduce((acc, num) => {
      return acc + num;
    }, 0);
    return sum;
  }
  get costDiscount() {
    const prices = this.products.map((product) => {
      return toNum(product.priceDiscount);
    });
    const sum = prices.reduce((acc, num) => {
      return acc + num;
    }, 0);
    return sum;
  }
  get discount() {
    return this.cost - this.costDiscount;
  }

  get priceUsual() {
    return this.cost;
  }
}

class Product {
  imageSrc;
  name;
  price;
  priceDiscount;
  constructor(card) {
    this.imageSrc = card.querySelector(
      ".main__shop-products-card-image"
    ).children[0].src;
    this.name = card.querySelector(".card__title").innerText;
    this.price = card.querySelector(".card__price--common-en").innerText;
    this.priceDiscount = card.querySelector(
      ".card__price--discount-en"
    ).innerText;
  }
}

const myCart = new Cart();

if (localStorage.getItem("cart") == null) {
  localStorage.setItem("cart", JSON.stringify(myCart));
}

const savedCart = JSON.parse(localStorage.getItem("cart"));
myCart.products = savedCart.products;
cartNum.textContent = myCart.count;

cardAddArr.forEach((cardAdd) => {
  cardAdd.addEventListener("click", (e) => {
    e.preventDefault();
    const card = e.target.closest(".main__shop-products-card");
    const product = new Product(card);
    myCart.addProduct(product);
    localStorage.setItem("cart", JSON.stringify(myCart));
    cartNum.textContent = myCart.count;
  });
});

const popup = document.querySelector(".header-section__popup");
const popupClose = document.querySelector("#popup_close");
const body = document.body;
const popupContainer = document.querySelector("#popup-container");
const popupProductList = document.querySelector("#popup_product_list");
const popupCost = document.querySelector("#popup_cost");
const popupDiscount = document.querySelector("#popup_discount");
const popupCostDiscount = document.querySelector("#popup_cost_discount");

cart.addEventListener("click", (e) => {
  e.preventDefault();
  document.body.style.overflow = "hidden";
  popup.classList.add("popup--open");
  body.classList.add("lock");
  popupContainerFill();
});

function popupContainerFill() {
  popupProductList.innerHTML = null;
  const savedCart = JSON.parse(localStorage.getItem("cart"));
  myCart.products = savedCart.products;
  const productsHTML = myCart.products.map((product) => {
    const productItem = document.createElement("div");
    productItem.classList.add("popup__product");

    const productWrap1 = document.createElement("div");
    productWrap1.classList.add("popup__product-wrap");
    const productWrap2 = document.createElement("div");
    productWrap2.classList.add("popup__product-wrap");

    const productImage = document.createElement("img");
    productImage.classList.add("popup__product-image");
    productImage.setAttribute("src", product.imageSrc);

    const productTitle = document.createElement("h2");
    productTitle.classList.add("popup__product-title");
    productTitle.innerHTML = product.name;

    const productPrice = document.createElement("div");
    productPrice.classList.add("popup__product-price");

    const usualPrice = product.price;

    productPrice.innerHTML = toCurrency(toNum(product.priceDiscount));

    const productDelete = document.createElement("button");
    productDelete.classList.add("popup__product-delete");
    productDelete.innerHTML = "&#10006;";

    productDelete.addEventListener("click", () => {
      myCart.removeProduct(product);
      localStorage.setItem("cart", JSON.stringify(myCart));
      cartNum.textContent = myCart.count;
      popupContainerFill();
    });

    productWrap1.appendChild(productImage);
    productWrap1.appendChild(productTitle);
    productWrap2.appendChild(productPrice);
    productWrap2.appendChild(productDelete);
    productItem.appendChild(productWrap1);
    productItem.appendChild(productWrap2);

    return productItem;
  });

  productsHTML.forEach((productHTML) => {
    popupProductList.appendChild(productHTML);
  });

  popupCost.value = toCurrency(myCart.cost);
  popupDiscount.value = toCurrency(myCart.discount);
  popupCostDiscount.value = toCurrency(myCart.costDiscount);
}

popupClose.addEventListener("click", (e) => {
  e.preventDefault();
  popup.classList.remove("popup--open");
  body.classList.remove("lock");
  document.body.style.overflow = "unset";
});

const clearCartButton = document.querySelector("#clear_cart");

clearCartButton.addEventListener("click", () => {
  myCart.products = [];

  localStorage.setItem("cart", JSON.stringify(myCart));

  cartNum.textContent = myCart.count;

  popupContainerFill();
});

const buyBtnHeader = document.querySelector(".header-section__buy-btn");

buyBtnHeader.addEventListener("click", function () {
  popup.style.display = "none";
  orderForm.style.display = "flex";
});
const clientName = document.querySelector("#name-input");
const clientNumber = document.querySelector("#number-input");

const butBtn = document.querySelector(".main__shop-products-order-btn");
butBtn.addEventListener("click", () => {
  orderForm.style.display = "none";

  const productList = myCart.products
    .map((product, index) => {
      return `Product ${index + 1}:
                Name: ${product.name}
                Price: ${product.price}
                Price Discount: ${product.priceDiscount}\n`;
    })
    .join("\n");

  localStorage.setItem("cart", JSON.stringify(myCart));
  const newInfoName = clientName.value;
  const newInfoNumber = clientNumber.value;
  const text = `Name: ${newInfoName}\nNumber: ${newInfoNumber} \nProducts: ${productList}`;
  const blob = new Blob([text], { type: "text/plain" });
  const link = document.createElement("a");
  link.download = "client-info.txt";
  link.href = window.URL.createObjectURL(blob);
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
  myCart.products = [];
  cartNum.textContent = myCart.count;
  document.body.style.overflow = "unset";
});

const justOrder = document.querySelector(".header-section__contact-btn");
const justInput = document.querySelector("#number-input2");

justOrder.addEventListener("click", () => {
  const newInfoNumber2 = justInput.value;
  const text = `Number: ${newInfoNumber2}`;
  const blob = new Blob([text], { type: "text/plain" });
  const link = document.createElement("a");
  link.download = "client-info.txt";
  link.href = window.URL.createObjectURL(blob);
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
});
