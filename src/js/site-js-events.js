/// /////////////////////////////////////////////
/// HEADER: CALL US
/// /////////////////////////////////////////////

const callButton = document.querySelector("#call-btn");
const callForm = document.querySelector("#contact-div");
const callHeadBtn = document.querySelector("#call-us-button");
const callHeadBtnAdapt = document.querySelector("#call-us-button-2");
const closeCallForm = document.querySelector(".header-section__contact-close");
const form2 = document.querySelector("#contactForm");

callHeadBtn.addEventListener("click", function () {
  callForm.style.display = "flex";
});

callHeadBtnAdapt.addEventListener("click", function () {
  callForm.style.display = "flex";
});

closeCallForm.addEventListener("click", function () {
  callForm.style.display = "none";
});

const inputElement2 = document.getElementById("number-input2");

callButton.addEventListener("click", function (event) {
  event.preventDefault();

  const numberInput2 = inputElement2.value;
  const numericValue2 = parseInt(numberInput2);

  badNumbers = [
    "!",
    "@",
    "#",
    "$",
    "%",
    "^",
    "&",
    "*",
    ".",
    "/",
    "?",
    ">",
    "<",
    "]",
    "[",
    "(",
    ")",
    "{",
    "}",
    "=",
  ];
  if (
    isNaN(numericValue2) ||
    badNumbers.some((char) => numberInput2.includes(char))
  ) {
    alert("Будь ласка, введіть коректний номер!");
  } else {
    form2.reset();
    callForm.style.display = "none";
  }
});

/// /////////////////////////////////////////////
/// HEADER: CATALOG
/// /////////////////////////////////////////////

const catalogBtn = document.querySelector(".header-section__button");
const catalogBtnAdpt = document.querySelector("#catalog-open-btn");
const catalogList = document.querySelector(".header-section__catalog");

catalogBtn.addEventListener("click", function () {
  if ((catalogList.style.display = "none")) {
    catalogList.style.display = "block";
  }
});

catalogList.addEventListener("click", function () {
  catalogList.style.display = "none";
});

catalogBtnAdpt.addEventListener("click", function () {
  if ((catalogList.style.display = "none")) {
    catalogList.style.display = "block";
  }
});

catalogList.addEventListener("click", function () {
  catalogList.style.display = "none";
});

/// /////////////////////////////////////////////
/// HEADER: CHANGE TO ANOTHER LANGUAGE/CURRENCY
/// /////////////////////////////////////////////

let lang = document.querySelector("#language-selector");
let curr = document.querySelector("#currency-selector");

lang.addEventListener("change", function () {
  let selectedOption = this.options[this.selectedIndex].value;
  if (selectedOption === "ua") {
    window.open("index.html", "_blank");
  } else if (selectedOption === "en") {
    window.open("index-english.html", "_blank");
  }
});

curr.addEventListener("change", function () {
  let selectedOption = this.options[this.selectedIndex].value;
  if (selectedOption === "ua-grivna") {
    window.open("index.html", "_blank");
  } else if (selectedOption === "ua-dollar") {
    window.open("index-curr-doll.html", "_blank");
  } else if (selectedOption === "us-grivna") {
    window.open("index-english.html", "_blank");
  } else if (selectedOption === "us-dollar") {
    window.open("index-english-curr-doll.html", "_blank");
  }
});

lang.addEventListener("change", function () {
  let selectedOption = this.options[this.selectedIndex].value;
  if (selectedOption === "ua-about-us") {
    window.open("about-us.html", "_blank");
  } else if (selectedOption === "en-about-us") {
    window.open("about-us-en.html", "_blank");
  }
});

lang.addEventListener("change", function () {
  let selectedOption = this.options[this.selectedIndex].value;
  if (selectedOption === "ua-contact-page") {
    window.open("contacts.html", "_blank");
  } else if (selectedOption === "en-contact-page") {
    window.open("contacts-en.html", "_blank");
  }
});

lang.addEventListener("change", function () {
  let selectedOption = this.options[this.selectedIndex].value;
  if (selectedOption === "payment-and-delivery-ua") {
    window.open("payment-and-delivery.html", "_blank");
  } else if (selectedOption === "payment-and-delivery-en") {
    window.open("payment-and-delivery-en.html", "_blank");
  }
});

lang.addEventListener("change", function () {
  let selectedOption = this.options[this.selectedIndex].value;
  if (selectedOption === "clients-reviews-ua") {
    window.open("clients-reviews.html", "_blank");
  } else if (selectedOption === "clients-reviews-en") {
    window.open("clients-reviews-en.html", "_blank");
  }
});

lang.addEventListener("change", function () {
  let selectedOption = this.options[this.selectedIndex].value;
  if (selectedOption === "our-guarantees-ua") {
    window.open("our-guarantees.html", "_blank");
  } else if (selectedOption === "our-guarantees-en") {
    window.open("our-guarantees-en.html", "_blank");
  }
});

lang.addEventListener("change", function () {
  let selectedOption = this.options[this.selectedIndex].value;
  if (selectedOption === "suppliers-and-intermediaries-ua") {
    window.open("suppliers-and-intermediaries.html", "_blank");
  } else if (selectedOption === "suppliers-and-intermediaries-en") {
    window.open("suppliers-and-intermediaries-en.html", "_blank");
  }
});
